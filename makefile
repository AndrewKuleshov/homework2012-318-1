.PHONY: clean

all: long.o main.o
        g++ long.o main.o -o all

long.o: long.h long.cpp
        g++ -c long.cpp

main.o: main.cpp
        g++ -c main.cpp

testall: long-test.h long-test.cpp
        g++ long-test.h long-test.cpp -o testall

clean:
        rm *.o
        rm all
        rm testall

