/*
 * Copyright KuleshovAndrew.
 * C++ program that works with long arithmetics.
 */

#include <cppunit/extensions/HelperMacros.h>
#include "long.h"

/**
 * Methods of Long.
 * 
 * @author Andrew Kuleshov
 * @file long-test.h
 * @since 16.11.2012
 */

class LongForTesting: public CPPUNIT_NS::TestFixture {
    Long *a,*b,*c;
    CPPUNIT_TEST_SUITE(LongForTesting);
    CPPUNIT_TEST(testAddWork);
    CPPUNIT_TEST(testAddPosSmallPosSmallResBig);
    CPPUNIT_TEST(testAddNegSmallNegBigResBig);
    CPPUNIT_TEST(testAddNegBigPosBigResZero);
    CPPUNIT_TEST(testAddZeroNegSmall);
    CPPUNIT_TEST(testSubtractWork);
    CPPUNIT_TEST(testSubtractNegBigNegSmallResSmall);
    CPPUNIT_TEST(testMultiplicateWork);
    CPPUNIT_TEST(testMultiplicatePosSmallNegSmallResBig);
    CPPUNIT_TEST(testMultiplicateNegSmallNegBigResBig);
    CPPUNIT_TEST(testMultiplicateNegBigZero);
    CPPUNIT_TEST(testMultiplicateOnePosBig);
    CPPUNIT_TEST(testDivideWork);
    CPPUNIT_TEST(testDividePosBigNegSmallResSmall);
    CPPUNIT_TEST(testDivideNegBigPosBigResBig);
    CPPUNIT_TEST(testDivideNegBigNegBigResOne);
    CPPUNIT_TEST(testDividePosBigZero);
    CPPUNIT_TEST(testRemainderWork);
    CPPUNIT_TEST(testRemainderZero);
    CPPUNIT_TEST(testAddEqual);
    CPPUNIT_TEST(testSubtractEqual);
    CPPUNIT_TEST(testMultiplicateEqual);
    CPPUNIT_TEST(testDivideEqual);
    CPPUNIT_TEST(testRemainderEqual);
    CPPUNIT_TEST(testPostIncrease);
    CPPUNIT_TEST(testPreIncrease);
    CPPUNIT_TEST(testPostDecrease);
    CPPUNIT_TEST(testPreDecrease);
    CPPUNIT_TEST(testCompareEqual);
    CPPUNIT_TEST(testCompareNotEqual);
    CPPUNIT_TEST(testCompareMore);
    CPPUNIT_TEST(testCompareLess);
    CPPUNIT_TEST(testCompareLessEqual);
    CPPUNIT_TEST(testCompareMoreEqual);
    CPPUNIT_TEST_SUITE_END();
public:

    /**
     * Method initializes three new objects of class Long.
     */
    void setUp();
    
    /**
     * Method deletes three objects of class Long.
     */
    void tearDown();
    
protected:

    /**
     * Method checks correctness of addition of two numbers.
     */
    void testAddWork();
    
    /**
     * Method checks correctness of addition of positive small
     * numbers that gives result of positive big number.
     */
    void testAddPosSmallPosSmallResBig();
    
    /**
     * Method checks correctness of addition of negative small
     * number and negative big number that gives result of negative
     * big number.
     */
    void testAddNegSmallNegBigResBig();
    
    /**
     * Method checks correctness of addition of negative big
     * number and positive big number that gives result zero.
     */
    void testAddNegBigPosBigResZero();
    
    /**
     * Method checks correctness of addition of negative small
     * number and zero.
     */
    void testAddZeroNegSmall();
    
    /**
     * Method checks correctness of subtraction of two numbers.
     */
    void testSubtractWork();
    
    /**
     * Method checks correctness of subtraction of negative big number
     * and negative small number that gives result of negative small
     * number.
     */
    void testSubtractNegBigNegSmallResSmall();
    
    /**
     * Method checks correctness of multiplication of two numbers.
     */
    void testMultiplicateWork();
    
    /**
     * Method checks correctness of multiplication of positive small
     * number and negative small number that gives result of negative
     * big number.
     */
    void testMultiplicatePosSmallNegSmallResBig();
    
    /**
     * Method checks correctness of multiplication of negative small
     * number and negative big number that gives result of positive
     * big number.
     */
    void testMultiplicateNegSmallNegBigResBig();
    
    /**
     * Method checks correctness of multiplication of negative big
     * number and zero that gives result of zero.
     */
    void testMultiplicateNegBigZero();
    
    /**
     * Method checks correctness of multiplication of one and positive
     * big number.
     */
    void testMultiplicateOnePosBig();
    
    /**
     * Method checks correctness of division of two numbers.
     */ 
    void testDivideWork();
    
    /**
     * Method checks correctness of division of positive big number and
     * negative small number that gives result of negative small number.
     */
    void testDividePosBigNegSmallResSmall();
    
    /**
     * Method checks correctness of division of negative big number and
     * positive big number that gives result of negative big number.
     */
    void testDivideNegBigPosBigResBig();
    
    /**
     * Method checks correctness of division of negative big number and
     * negative big number that gives result of zero.
     */
    void testDivideNegBigNegBigResOne();
    
    /**
     * Method checks correctness of division of positive big number and
     * zero that throws exception.
     */
    void testDividePosBigZero();
    
    /**
     * Method checks correctness of computing remainder of two numbers.
     */
    void testRemainderWork();
    
    /**
     * Method checks correctness of computing remainder of two numbers
     * if equals zero.
     */
    void testRemainderZero();
    
    /**
     * Method checks correctness of addition of two numbers and
     * assignment to first number.
     */
    void testAddEqual();
    
    /**
     * Method checks correctness of subtraction of two numbers and
     * assignment to first number.
     */
    void testSubtractEqual();
    
    /**
     * Method checks correctness of multiplication of two numbers and
     * assignment to first number.
     */
    void testMultiplicateEqual();
    
    /**
     * Method checks correctness of division of two numbers and
     * assignment to first number.
     */
    void testDivideEqual();
    
    /**
     * Method checks correctness of calculating remainder of two numbers 
     * and assignment to first number.
     */
    void testRemainderEqual();
    
    /**
     * Method checks correctness of postfix increasing by one.
     */
    void testPostIncrease();
    
    /**
     * Method checks correctness of prefix increasing by one.
     */
    void testPreIncrease();
    
    /**
     * Method checks correctness of postfix decreasing by one.
     */
    void testPostDecrease();
    
    /**
     * Method checks correctness of prefix decreasing by one.
     */
    void testPreDecrease();
    
    /**
     * Method checks correctness of comparing two numbers if equal.
     */
    void testCompareEqual();
    
    /**
     * Method checks correctness of comparing two numbers if not equal.
     */
    void testCompareNotEqual();
    
    /**
     * Method checks correctness of comparing two numbers if greater.
     */
    void testCompareMore();
    
    /**
     * Method checks correctness of comparing two numbers if less.
     */
    void testCompareLess();
    
    /**
     * Method checks correctness of comparing two numbers if greater
     * or equal.
     */
    void testCompareMoreEqual();
    
    /**
     * Method checks correctness of comparing two numbers if less or 
     * equal.
     */
    void testCompareLessEqual();
};
