/*
 * Copyright KuleshovAndrew 
 * C++ program computing long arithmetics.
 */

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "long-test.h"

CPPUNIT_TEST_SUITE_REGISTRATION(LongForTesting);
/**
 * Method initializes three new objects of class Long.
 * 
 * @author Andrew Kuleshov
 * @file long-test.cpp
 * @since 16.11.2012
 */
void LongForTesting::setUp() {
    a = new Long;
    b = new Long(0);
    c = new Long(10000);
}

/**
 * Method deletes three objects of class Long.
 */
void LongForTesting::tearDown() {
    delete a;
    delete b;
    delete c;
}

/**
 * Method checks correctness of addition of two numbers.
 */
void LongForTesting::testAddWork() {
    *a = 6;
    *b = 4;
    *c = 10;
    CPPUNIT_ASSERT(*a + *b == *c);
}

/**
 * Method checks correctness of addition of positive small
 * numbers that gives result of positive big number.
 */
void LongForTesting::testAddPosSmallPosSmallResBig() {
    *a = 6666;
    *b = 3334;
    *c = 10000;
    CPPUNIT_ASSERT(*a + *b == *c);
}

/**
 * Method checks correctness of addition of negative small
 * number and negative big number that gives result of negative
 * big number.
 */
void LongForTesting::testAddNegSmallNegBigResBig() {
    *a = -19666;
    *b = -334;
    *c = -20000;
    CPPUNIT_ASSERT(*a + *b == *c);
}

/**
 * Method checks correctness of addition of negative big
 * number and positive big number that gives result zero.
 */
void LongForTesting::testAddNegBigPosBigResZero() {
    *a = -10000;
    *b = 10000;
    *c = 0;
    CPPUNIT_ASSERT(*a + *b == *c);
}

/**
 * Method checks correctness of addition of negative small
 * number and zero.
 */
void LongForTesting::testAddZeroNegSmall() {
    *a = 0;
    *b = -10;
    *c = -10;
    CPPUNIT_ASSERT(*a + *b == *c);
}

/**
 * Method checks correctness of subtraction of two numbers.
 */
void LongForTesting::testSubtractWork() {
    *a = 10;
    *b = 4;
    *c = 6;
    CPPUNIT_ASSERT(*a - *b == *c);
}

/**
 * Method checks correctness of subtraction of negative big number
 * and negative small number that gives result of negative small
 * number.
 */
void LongForTesting::testSubtractNegBigNegSmallResSmall() {
    *a = -10000;
    *b = -1;
    *c = -9999;
    CPPUNIT_ASSERT(*a - *b == *c);
}

/**
 * Method checks correctness of multiplication of two numbers.
 */
void LongForTesting::testMultiplicateWork() {
    *a = 7;
    *b = 3;
    *c = 21;
    CPPUNIT_ASSERT(*a * *b == *c);
}

/**
 * Method checks correctness of multiplication of positive small
 * number and negative small number that gives result of negative
 * big number.
 */
void LongForTesting::testMultiplicatePosSmallNegSmallResBig() {
    *a = 10;
    *b = -1000;
    *c = -10000;
    CPPUNIT_ASSERT(*a * *b == *c);
}

/**
 * Method checks correctness of multiplication of negative small
 * number and negative big number that gives result of positive
 * big number.
 */
void LongForTesting::testMultiplicateNegSmallNegBigResBig() {
    *a = -10;
    *b = -10000000;
    *c = 100000000;
    CPPUNIT_ASSERT(*a * *b == *c);
}

/**
 * Method checks correctness of multiplication of negative big
 * number and zero that gives result of zero.
 */
void LongForTesting::testMultiplicateNegBigZero() {
    *a = -10000;
    *b = 0;
    *c = 0;
    CPPUNIT_ASSERT(*a * *b == *c);
}

/**
 * Method checks correctness of multiplication of one and positive
 * big number.
 */
void LongForTesting::testMultiplicateOnePosBig() {
    *a = 1;
    *b = 10000;
    *c = 10000;
    CPPUNIT_ASSERT(*a * *b == *c);
}

/**
 * Method checks correctness of division of two numbers.
 */ 
void LongForTesting::testDivideWork() {
    *a = 24;
    *b = 6;
    *c = 4;
    CPPUNIT_ASSERT(*a / *b == *c);
}

/**
 * Method checks correctness of division of positive big number and
 * negative small number that gives result of negative small number.
 */
void LongForTesting::testDividePosBigNegSmallResSmall() {
    *a = 10000;
    *b = -1000;
    *c = -10;
    CPPUNIT_ASSERT(*a / *b == *c);
}

/**
 * Method checks correctness of division of negative big number and
 * positive big number that gives result of negative big number.
 */
void LongForTesting::testDivideNegBigPosBigResBig() {
    *a = -100000000;
    *b = 10000;
    *c = -10000;
    CPPUNIT_ASSERT(*a / *b == *c);
}

/**
 * Method checks correctness of division of negative big number and
 * negative big number that gives result of zero.
 */
void LongForTesting::testDivideNegBigNegBigResOne() {
    *a = -100000000;
    *b = -100000000;
    *c = 1;
    CPPUNIT_ASSERT(*a / *b == *c);
}

/**
 * Method checks correctness of division of positive big number and
 * zero that throws exception.
 */
void LongForTesting::testDividePosBigZero() {
    *a = 10000;
    *b = 0;
    CPPUNIT_ASSERT_THROW(*a / *b, char *);
}

/**
 * Method checks correctness of computing remainder of two numbers.
 */
void LongForTesting::testRemainderWork() {
    *a = 10;
    *b = 3;
    *c = 1;
    CPPUNIT_ASSERT(*a % *b == *c);
}

/**
 * Method checks correctness of computing remainder of two numbers
 * if equals zero.
 */
void LongForTesting::testRemainderZero() {
    *a = 100000000;
    *b = 10000;
    *c = 0;
    CPPUNIT_ASSERT(*a % *b == *c);
}

/**
 * Method checks correctness of addition of two numbers and
 * assignment to first number.
 */
void LongForTesting::testAddEqual() {
    *a = 6666;
    *b = 3334;
    *a += *b;
    CPPUNIT_ASSERT(*a == 10000);
}

/**
 * Method checks correctness of subtraction of two numbers and
 * assignment to first number.
 */
void LongForTesting::testSubtractEqual() {
    *a = 17777;
    *b = 7777;
    *a -= *b;
    CPPUNIT_ASSERT(*a == 10000);
}

/**
 * Method checks correctness of multiplication of two numbers and
 * assignment to first number.
 */
void LongForTesting::testMultiplicateEqual() {
    *a = 1000;
    *b = 10;
    *a *= *b;
    CPPUNIT_ASSERT(*a == 10000);
}

/**
 * Method checks correctness of division of two numbers and
 * assignment to first number.
 */
void LongForTesting::testDivideEqual() {
    *a = 100000000;
    *b = 10000;
    *a /= *b;
    CPPUNIT_ASSERT(*a == 10000);
}

/**
 * Method checks correctness of calculating remainder of two numbers 
 * and assignment to first number.
 */
void LongForTesting::testRemainderEqual() {
    *a = 1000010000;
    *b = 100000;
    *a %= *b;
    CPPUNIT_ASSERT(*a == 10000);
}

/**
 * Method checks correctness of postfix increasing by one.
 */
void LongForTesting::testPostIncrease() {
    *a = 9999;
    *b = (*a)++;
    CPPUNIT_ASSERT(*b == 9999);
}

/**
 * Method checks correctness of prefix increasing by one.
 */
void LongForTesting::testPreIncrease() {
    *a = 9999;
    *b = ++(*a);
    CPPUNIT_ASSERT(*b == 10000);
}

/**
 * Method checks correctness of postfix decreasing by one.
 */
void LongForTesting::testPostDecrease() {
    *a = 10000;
    *b = (*a)--;
    CPPUNIT_ASSERT(*b == 10000);
}

/**
 * Method checks correctness of prefix decreasing by one.
 */
void LongForTesting::testPreDecrease() {
    *a = 10000;
    *b = --(*a);
    CPPUNIT_ASSERT(*b == 9999);
}

/**
 * Method checks correctness of comparing two numbers if equal.
 */
void LongForTesting::testCompareEqual() {
    *a = 77777;
    *b = 77777;
    CPPUNIT_ASSERT(*a == *b);
}

/**
 * Method checks correctness of comparing two numbers if not equal.
 */
void LongForTesting::testCompareNotEqual() {
    *a = 77777;
    *b = 77770;
    CPPUNIT_ASSERT(*a != *b);
}

/**
 * Method checks correctness of comparing two numbers if greater.
 */
void LongForTesting::testCompareMore() {
    *a = 77777;
    *b = 7777;
    CPPUNIT_ASSERT(*a > *b);
}

 
void LongForTesting::testCompareLess() {
    *a = 77770;
    *b = 77777;
    CPPUNIT_ASSERT(*a < *b);
}

/**
 * Method checks correctness of comparing two numbers if greater
 * or equal.
 */
void LongForTesting::testCompareMoreEqual() {
    *a = 77777;
    *b = 77777;
    CPPUNIT_ASSERT(*a >= *b);
}

/**
 * Method checks correctness of comparing two numbers if less or 
 * equal.
 */
void LongForTesting::testCompareLessEqual() {
    *a = 77770;
    *b = 77777;
    CPPUNIT_ASSERT(*a <= *b);
}

/**
 * Main program initializes tests.
 */
int main() {
    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest(registry.makeTest());
    runner.run();
    return 0;
}
