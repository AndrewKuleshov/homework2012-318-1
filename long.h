/*
 * Copyright KuleshovAndrew.
 * C++ program that works with long arithmetics.
 */

#include <iostream>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#define BASE 8
#define Max_Num 999999999
#define Base_Num 1000000000


using namespace std;

/**
 * Class Long with fields such as: vector from <int> (number information),
 * integer base (technical information) and boolean sign.
 * 
 * @author Andrew Kuleshov
 * @file long.h
 * @since 16.11.2012
 */
class Long {
    vector <int> numbers;
    int base;
    bool sign;
public:
   
    /**
     * Constructor of class (default constructor) with no parameters.
     */
    Long ();

    /**
     * Convertion constructor from int to Long.
     *
     * @param  number integer, that will be converted
     */
    Long (int number);

    /**
     * Operator for assignment of one Long number to another .
     *
     * @param  number that value will be assigned
     * @return assigned Long number
     */

    Long& operator = (Long number);
    /**
     * Method that compares absolute value of two Long numbers 
     *
     * @param  y const Long & that will be compared
     * @return true if sourse number is more than second, else false
     */

    bool abs_compare_more (const Long & y);
    
   
    /**
     * Friend function for output of Long number.
     *
     * @param  output stream for output of Long number
     * @param  a Long number to output
     * @return stream with output Long number
     */
    friend ostream& operator << (ostream &output, Long a);

     /**
     * Friend operator for input of Long number.
     *
     * @param  input stream for input of Long number
     * @param  a Long number to input
     * @return stream with input Long number
     */
    friend istream& operator >> (istream &input, Long &a);
    
    /**
     * Friend operator, comparing two Long numbers for being greater.
     *
     * @param  a first const Long & number to compare
     * @param  b second  const Long & number to compare
     * @return true if a is greater than b, else returns false
     */
    friend bool operator > (const Long &a, const Long &b);
    
    /**
     * Friend operator, comparing two Long numbers for being greater or equal.
     *
     * @param  a first const Long & number to compare
     * @param  b second const Long & Long number to compare
     * @return true if number a is equal to b or is greater than b, else returns false
     */
    friend bool operator >= (const Long &a, const Long &b);

    /**
     * Friend operator, comparing two Long numbers if one of them 
     * is less than another.
     *
     * @param  a first const Long & Long number to compare
     * @param  b second const Long & Long number to compare
     * @return true if number a is less than b, else returns false
     */
    friend bool operator < (const Long &a, const Long &b);
    
    /**
     * Friend operator for comparing two Long numbers for less or equal.
     *
     * @param  a first const Long & Long number to compare
     * @param  b second const Long & Long number to compare
     * @return true if a is equal to b or a is less than b, else returns false
     */
    friend bool operator <= (const Long &a, const Long &b);
    
    /**
     * Friend operator: Equal comparing .
     *
     * @param  a first const Long & number to compare
     * @param  b second const Long & number to compare
     * @return true if a equal to b, else returns false
     */
    friend bool operator == (const Long &a, const Long &b);

    /**
     * Friend operator for comparing two Long numbers if not equal.
     *
     * @param  a first const Long & Long number to compare
     * @param  b second const Long & Long number to compare
     * @return true if a is not equal to b, else returns false
     */

    friend bool operator != (const Long &a, const Long &b);
    
    /**
     * Friend operator to change a sign of Long number to opposite
     *
     * @param  number to negate a Long number
     * @return negated Long number
     */
    friend Long operator - (Long a);

    /**
     * Friend operator that sums two Long numbers.
     *
     * @param  a first const Long & number to add
     * @param  b second const Long & number to add
     * @return sum of a and b
     */
    friend Long operator + (const Long &a, const Long &b);

    /**
     * Friend operator that is searching a difference between two Long numbers.
     *
     * @param  a first const Long & number - minuend
     * @param  b second const Long & number -subtrahend
     * @return difference between a and b
     */ 
    friend Long operator - (const Long &a, const Long &b);
    
    /**
     * Friend operator that is searching product of two Long numbers.
     *
     * @param  a first  multiplied const Long &
     * @param  b second multiplied const Long & 
     * @return product of a and b
     */

    friend Long operator * (const Long &a, const Long &b);

    /**
     * Friend operator that is searching quotient of two Long numbers.
     *
     * @param  a first const Long & number - dividend
     * @param  b second const Long & number - divider
     * @return quotient of a and b
     */

    friend Long operator / (const Long &a, const Long &b);
    
    /**
     * Friend operator for searching sum and for assignment 
     * result to sourse number.
     *
     * @param  a Long number - first summand
     * @param  b Long number - second summand
     * @return result, assigned to a
     */

    friend Long operator += (Long a, Long b);
   
    /**
     * Friend operator for searching difference between two Long numbers  
     * and for assignment of result to sourse number.
     *
     * @param  a first Long number to subtract
     * @param  b second Long number to subtract
     * @return difference of a and b assigned to a
     */
    friend Long operator -= (Long a, Long b);
    
    /**
     * Friend operator for searching product of two Long numbers and 
     * for assignment of result to sourse number.
     *
     * @param  a first  multiplied Long
     * @param  b second multiplied Long
     * @return product of a and b assigned to a
     */

    friend Long operator *= (Long a, Long b);\
    
    /**
     * Friend operator for  quotient of two Long numbers and 
     * assignment to first number.
     *
     * @param  a first const Long & number - dividend
     * @param  b second const Long & number - divider
     * @return quotient of a and b assigned to a
     */

    friend Long operator /= (Long a, Long b);
    
    /**
     * Friend operator - prefix increment 
     *
     * @param  a number that is increased 
     * @return result of increment
     */

    friend Long operator ++ (Long a);
     
    /**
     * Friend operator - postfix increment
     *
     * @param  a number that is increased 
     * @param  x - formal technical parametr
     * @return sourse number
     */
    friend Long operator ++ (Long a, int x);

    /**
     * Friend operator - prefix decrement
     *
     * @param  a number that isdecreased 
     * @return decreased a
     */
    friend Long operator -- (Long a);
    
    /**
     * Friend operator - postfix decrement
     *
     * @param  a number that is decreased 
     * @param  x - formal technical parametr
     * @return sourse number
     */

    friend Long operator -- (Long a, int x);

    /**
     * Friend operator for searching remainder of division.
     *
     * @param  a first const Long number - dividend
     * @param  b second const Long number - divider
     * @return remainder of division of a and b
     */

    friend Long operator % (Long a, Long b);
    
    /**
     * Friend operator for searching remainder of division and for
     * assignment a result
     * @param  a first const Long number - dividend
     * @param  b second const Long number - divider
     * @return remainder of division of a and b
     */

    friend Long operator %= (Long a, Long b);
    
    /**
     * Friend function for computing pair of quotient and remainder
     * of division of two Long numbers.
     *
     * @param  a first const Long number - dividend
     * @param  b second const Long number - divider
     * @see    operator /
     * @see    operator %
     * @return pair of Long numbers (result of division)
     */

    friend pair <Long, Long> divmod (Long a, Long b);

};

