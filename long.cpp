/*
 * Copyright KuleshovAndrew.
 * C++ program that works with long arithmetics.
 */

#include "long.h"
#define BASE 8

using namespace std;

/**
 * Methods of Long.
 * 
 * @author Andrew Kuleshov
 * @file long.cpp
 * @since 16.11.2012
 */

/**
 * Method that compares absolute value of two Long numbers 
 *
 * @param  y const Long & that will be compared
 * @return true if sourse number is more than second, else false
 */

bool Long:: abs_compare_more (const Long & y){
    if (numbers.size() > y.numbers.size())
         return 1;
    if (numbers.size() < y.numbers.size())
         return 0;

    for (int i = 0; i < numbers.size (); ++i) {
         if (numbers[i] > y.numbers[i]) return 1;
    }

return 0;
}

/**
 * Friend operator - postfix increment
 *
 * @param  a number that is increased 
 * @param  x - formal technical parametr
 * @return sourse number
 */

Long operator ++ (Long a, int b) {
    Long number = a;
    a += 1;
    return number;
}

/**
 * Friend operator - prefix increment 
 *
 * @param  a number that is increased 
 * @return result of increment
 */

Long operator ++ (Long a) {
    return a + 1;
}

/**
 * Friend operator - postfix decrement
 *
 * @param  a number that is decreased 
 * @param  x - formal technical parametr
 * @return sourse number
 */

Long operator -- (Long a, int b) {
    Long number = a;
    a -= 1;
    return number;
}

/**
 * Friend operator - prefix decrement
 *
 * @param  a number that isdecreased 
 * @return decreased a
 */
Long operator -- (Long a) {
    return a - 1;
}

/**
 * Friend operator that is searching a difference between two Long numbers.
 *
 * @param  a first const Long & number - minuend
 * @param  b second const Long & number -subtrahend
 * @return difference between a and b
 */
 
Long operator - (const Long & number1, const Long &   number2)
{
    Long sub = number2;
    Long result;
    sub.sign = !sub.sign;
    return number1 + sub;
   /* int operation_flag = 0, res;
    if ((number1.sign == number2.sign ))
    {
        if (number1.abs_compare_more(number2)) {
            //cout<< "WORKING"<<endl;
            int size_max = (number1.vector_contain).size(), size_min = (number2.vector_contain).size();
            result.sign = number1.sign;
            (result.vector_contain).resize(size_max,0);
            while (1)
{
                if (size_max < 0) break;
                    --size_max;
                    --size_min;

                if (size_min < 0) result.vector_contain[size_max] = number1.vector_contain[size_max] - operation_flag;
                else
                {
                    res = (number1.vector_contain [size_max] -  number2.vector_contain [size_min] - operation_flag);
                    if (res < 0)
                    {
                        operation_flag = 1;
                        res = Base_Num + res;
                    }
                    else operation_flag = 0;

                    result.vector_contain [size_max] = res;

                }
            }

                res = result.vector_contain [(result.vector_contain).size()-1];
                cout<< result.vector_contain[0] << result.vector_contain [1] << endl;
                int j = 0;
                while (res > 0)
                {
                    res = res/10;
                    ++j;
                }
                result.real_number_in_last_cell = j;
                result.number_of_integers_in_last_cell = number1.number_of_integers_in_last_cell;


        }
    }
    */
    //cout<< "MAde"<<endl;
}

/**
 * Friend operator, comparing two Long numbers if one of them 
 * is less than another.
 *
 * @param  a first const Long & Long number to compare
 * @param  b second const Long & Long number to compare
 * @return true if number a is less than b, else returns false
 */

bool operator < (const Long &  a, const Long &  b) {
    if ((a.sign == 1) && (b.sign == 0)) {
        return 1;
    }
    if ((a.sign == 0) && (b.sign == 1)) {
        return 0;
    }
    if ((a.sign == b.sign) && (a.sign == 0)) {
        if (a.numbers.size() > b.numbers.size()) {
            return 0;
        } else if (a.numbers.size() < b.numbers.size()) {
            return 1;
        } else {
            for (int i = a.numbers.size()-1; i >= 0; i--) {
                if (a.numbers[i] > b.numbers[i]) {
                    return 0;
                }
                if (a.numbers[i] < b.numbers[i]) {
                    return 1;
                }
            }
            return 0;
        }
    }
    if ((a.sign == b.sign) && (a.sign == 1)) {
        if (a.numbers.size() > b.numbers.size()) {
            return 1;
        } else if (a.numbers.size() < b.numbers.size()) {
            return 0;
        } else {
            for (int i = a.numbers.size()-1; i >= 0; i--) {
                if (a.numbers[i] > b.numbers[i]) {
                    return 1;
                }
                if (a.numbers[i] < b.numbers[i]) {
                    return 0;
                }
            }
            return 0;
        }
    }
    cerr << "Error: wrong comparison." << endl;
    exit(1);
}

/**
 * Friend operator, comparing two Long numbers for being greater.
 *
 * @param  a first const Long & number to compare
 * @param  b second  const Long & number to compare
 * @return true if a is greater than b, else returns false
 */

bool operator > (const Long &  a, const Long & b){
    return (!(a < b) && (a != b));
}

/**
 * Friend operator for comparing two Long numbers for less or equal.
 *
 * @param  a first const Long & Long number to compare
 * @param  b second const Long & Long number to compare
 * @return true if a is equal to b or a is less than b, else returns false
 */

bool operator <= (const Long &  a, const Long &  b){
    return ((a < b) || (a == b));
}

/**
 * Friend operator, comparing two Long numbers for being greater or equal.
 *
 * @param  a first const Long & number to compare
 * @param  b second const Long & Long number to compare
 * @return true if number a is equal to b or is greater than b, else returns false
 */

bool operator >= (const Long &  a, const Long &  b) {
    return !(a < b);
}

/**
 * Friend operator: Equal comparing .
 *
 * @param  a first const Long & number to compare
 * @param  b second const Long & number to compare
 * @return true if a equal to b, else returns false
 */

bool operator == (const Long &  a, const Long &  b) {
    if (a.sign != b.sign) {
        return 0;
    }
    if (a.numbers.size() != b.numbers.size()) {
        return 0;
    }
    for (int i = a.numbers.size()-1; i >= 0; i--) {
        if (a.numbers[i] != b.numbers[i]) {
            return 0;
        }
    }
    return 1;
}

/**
 * Friend operator that sums two Long numbers.
 *
 * @param  a first const Long & number to add
 * @param  b second const Long & number to add
 * @return sum of a and b
 */

Long operator + (const Long &a, const Long &b) {
    bool temp = 0;
    Long sum, sub;
    if (a.sign == b.sign) {
        sum = a;
        while (b.numbers.size() > sum.numbers.size()) {
            sum.numbers.push_back(0);
        }
        for (int i = 0; i < sum.numbers.size(); i++) {
            if (i < b.numbers.size()) {
                sum.numbers[i] += b.numbers[i] + temp;
            } else {
                sum.numbers[i] += temp;
            }
            if (sum.numbers[i] >= 10000) {
                temp = 1;
                sum.numbers[i] -= a.base;
            } else {
                temp = 0;
            }
            if ((temp == 1) && (i == sum.numbers.size()-1)) {
                sum.numbers.push_back(1);
                break;
            }
        }
    } else {
        if (a.numbers.size() > b.numbers.size()) {
            sum = a;
            sub = b;
        } else if (a.numbers.size() < b.numbers.size()) {
            sum = b;
            sub = a;
        } else {
            for (int i = a.numbers.size()-1; i >= 0; i--) {
                if (a.numbers[i] > b.numbers[i]) {
                    sum = a;
                    sub = b;
                    break;
                }
                if (a.numbers[i] < b.numbers[i]) {
                    sum = b;
                    sub = a;
                    break;
                }
            }
        }
        for (int i = 0; i < sub.numbers.size(); i++) {
            if (i < sub.numbers.size()) {
                sum.numbers[i] -= sub.numbers[i] + temp;
            } else {
                sum.numbers[i] -= temp;
            }
            if (sum.numbers[i] < 0) {
                temp = 1;
                sum.numbers[i] += a.base;
            } else {
                temp = 0;
            }
        }
        while ((sum.numbers.back() == 0) && (sum.numbers.size() != 0)) {
            sum.numbers.pop_back();
        }
    }
    return sum;
}




/**
 * Friend operator to change a sign of Long number to opposite
 *
 * @param  number to negate a Long number
 * @return negated Long number
 */

Long operator - (Long x)
{
    if (x.sign == 1) x.sign = 0;
    else x.sign = 1;
    return x;
}


/**
 * Operator for assignment of one Long number to another .
 *
 * @param  number that value will be assigned
 * @return assigned Long number
 */

Long & Long :: operator = (Long  x)
{
    numbers.clear();
    sign = x.sign;
    for (int it = 0; it < ((x.numbers)).size(); ++it)
        {
            numbers.push_back (x.numbers [it]);
        }
    return *this;
}



/**
 * Friend operator for searching sum and for assignment 
 * result to sourse number.
 *
 * @param  a Long number - first summand
 * @param  b Long number - second summand
 * @return result, assigned to a
 */

Long operator += (Long a, Long b) {
    return a + b;
}

/**
 * Friend operator for searching difference between two Long numbers  
 * and for assignment of result to sourse number.
 *
 * @param  a first Long number to subtract
 * @param  b second Long number to subtract
 * @return difference of a and b assigned to a
 */

Long operator -= (Long a, Long b) {
    return a - b;
}

/**
 * Constructor of class (default constructor) with no parameters.
 */

Long :: Long (){
    ((*this).numbers).clear ();
    (*this).sign = 0;
    (*this).base = 10000;

}

/**
 * Convertion constructor from int to Long.
 *
 * @param  number integer, that will be converted
 */

Long::Long (int a){
    numbers.clear();
    base = 10000;
    sign = 0;
    if (a < 0) {
    sign = 1;
    a = -a;
    }
    while (1){
        if (a <= 0) break;
        numbers.push_back (a % base);
        a = a / base;
    }
}

/**
 * Friend operator for comparing two Long numbers if not equal.
 *
 * @param  a first const Long & Long number to compare
 * @param  b second const Long & Long number to compare
 * @return true if a is not equal to b, else returns false
 */

bool operator != (const Long & a, const Long &  b) {
    return !(a == b);;
}


/**
 * Friend operator for input of Long number.
 *
 * @param  input stream for input of Long number
 * @param  a Long number to input
 * @return stream with input Long number
 */
 istream& operator >> (istream& input, Long &number) {
    
    char c = getchar();
    string longs;
    if (c == '-') {
        number.sign = 1;
        c = getchar();
    }
    while (c == '0') {
        c = getchar();
    }
    if ((c == '\n') || (c == EOF)) {
        cerr << "Nothing in input" << endl;
        exit(1);
    }
    while ((c != EOF) && (c != '\n')) {
        if (isdigit(c)) {
            longs = longs + c;
        } else {
            cerr << "Wrong symbol: " << c << endl;
            exit(1);
        }
        c = getchar();
    }
    int length = longs.length();
    if (length > 10000) {
        cerr << "Number is too long." << endl;
        exit(1);
    }
    for (int i = length; i > 0; i -= 4) {
        if (i < 4) {
            number.numbers.push_back (atoi (longs.substr(0, i).c_str()));
        } else {
            number.numbers.push_back (atoi (longs.substr(i - 4, 4).c_str()));
        }
    }
    return input;
}

/**
 * Friend function for output of Long number.
 *
 * @param  output stream for output of Long number
 * @param  a Long number to output
 * @return stream with output Long number
 */

ostream& operator << (ostream& output, Long number) {

    if (number.sign == 1) {
        cout << '-';
    }
    cout << number.numbers[number.numbers.size()-1];
    for (int i = number.numbers.size()-2; i >= 0; i--) {
        printf("%04d", number.numbers[i]);
    }
    cout << endl;
    return output;
}

/**
 * Friend operator for searching sum and for assignment 
 * result to sourse number.
 *
 * @param  a Long number - first summand
 * @param  b Long number - second summand
 * @return result, assigned to a
 */

Long operator +=(Long& a, Long& b){
        a =  a + b;
        return a;
}

/**
 * Friend operator for searching difference between two Long numbers  
 * and for assignment of result to sourse number.
 *
 * @param  a first Long number to subtract
 * @param  b second Long number to subtract
 * @return difference of a and b assigned to a
 */
Long operator -=(Long& a, Long& b){
        a =  a - b;
        return a;
}

/**
 * Friend operator for searching product of two Long numbers and 
 * for assignment of result to sourse number.
 *
 * @param  a first  multiplied Long
 * @param  b second multiplied Long
 * @return product of a and b assigned to a
 */

Long operator *= (Long a, Long b) {
    return a * b;
}



/**
 * Friend operator that is searching product of two Long numbers.
 *
 * @param  a first  multiplied const Long &
 * @param  b second multiplied const Long & 
 * @return product of a and b
 */

Long operator * (const Long & a, const Long & b) {
    Long mult;
    mult.numbers.resize((a.numbers.size() + b.numbers.size()), 0);
    for (int i = 0; i < a.numbers.size(); i++) {
        int temp = 0;
        for (int j = 0; j < b.numbers.size() || temp; j++) {
            int now = mult.numbers[i+j] + a.numbers[i] * 1ll * (j < (b.numbers.size()) ? b.numbers[j] : 0) + temp;
            mult.numbers[i+j] = now % a.base;
            temp = now / a.base;
            if ((temp == 1) && (i == mult.numbers.size()-1)) {
                mult.numbers.push_back(1);
                break;
            }
        }
        while ((mult.numbers.back() == 0) && (mult.numbers.size() != 0)) {
            mult.numbers.pop_back();
        }
    }
    mult.sign = (a.sign + b.sign) % 2;
    return mult;
}

/**
 * Friend function for computing pair of quotient and remainder
 * of division of two Long numbers.
 *
 * @param  a first const Long number - dividend
 * @param  b second const Long number - divider
 * @see    operator /
 * @see    operator %
 * @return pair of Long numbers (result of division)
 */

pair<Long, Long> divmod ( Long&a,  Long&b){
        pair <Long, Long> res;
        res.first = a/b;
        res.second = a%b;
        return res;
}

/**
 * Friend operator that is searching quotient of two Long numbers.
 *
 * @param  a first const Long & number - dividend
 * @param  b second const Long & number - divider
 * @return quotient of a and b
 */

Long operator / (const Long & x, const Long & y) {
    int sign = (x.sign + y.sign) % 2;
    Long a = x, b = y, div, val = 0;
    a.sign = 0;
    b.sign = 0;
    if (b == 0) {
        cerr << "Division by zero." << endl;
        exit(1);
    }
    if (a < b) {
        return 0;
    }
    if (a == b) {
        if (sign) {
            return -1;
        } else {
            return 1;
        }
    }
    if (b.numbers.size() == 1) {
        Long mult = a;
        int temp = 0, save = b.numbers[0];
        for (int i = mult.numbers.size()-1; i >= 0; i--) {
             int now = mult.numbers[i] + temp * 1ll * a.base;
             mult.numbers[i] = now / save;
             temp = now % save;
        }
        while ((mult.numbers.size() != 0) && (mult.numbers.back() == 0)) {
             mult.numbers.pop_back();
        }
        mult.sign = sign;
        return mult;
    }
    div.numbers.resize(a.numbers.size());
    for (int i = a.numbers.size()-1; i >= 0; i--) {
         int x = 0, y = 0;
         val *= a.base;
         val.numbers[0] = a.numbers[i];
         while (y <= a.base) {
             int z = (y + a.base) >> 1;
             if (b * z <= val) {
                 x = z;
                 y = z + 1;
             } else {
                 a.base = z - 1;
             }
         }
         div.numbers[i] = x;
         if (b * x == val) {
             val = 0;
         } else {
             val -= b * x;
         }
    }
    div.sign = sign;
    while ((div.numbers.size() != 0) && (div.numbers.back() == 0)) {
         div.numbers.pop_back();
    }
    return div;
}

/**
 * Friend operator for searching remainder of division.
 *
 * @param  a first const Long number - dividend
 * @param  b second const Long number - divider
 * @return remainder of division of a and b
 */

Long operator % ( Long  a,  Long  b){
    Long div, temp;
    div = a / b;
    temp = a - div * b;
    while ((temp.numbers.size() != 0) && (temp.numbers.back() == 0)) {
        temp.numbers.pop_back();
    }
    return temp;
}

/**
 * Friend operator for searching quotient of two Long numbers and 
 * assignment to first number.
 *
 * @param  a first const Long & number - dividend
 * @param  b second const Long & number - divider
 * @return quotient of a and b assigned to a
 */

Long operator /= (Long a, Long b){
    return a / b;
}

/**
 * Friend operator for searching remainder of division and for
 * assignment a result
 * @param  a first const Long number - dividend
 * @param  b second const Long number - divider
 * @return remainder of division of a and b
 */
Long operator %= (Long a, Long b){
    return a % b;
}
